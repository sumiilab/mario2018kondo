{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ViewPatterns #-}
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Text.Printf
import Graphics.Gloss.Juicy

import Data.Maybe (isJust)

import Debug.Trace

data World = World {ball :: Ball, blocks :: [Block], enemies :: [Ball], passed :: Float, pict :: Picture, pictbg :: Picture, score :: Int, keyPressed :: Maybe Direction }

data Block = Block { bx :: Float,
                     by :: Float,
                     bh :: Float,
                     bw :: Float,
                     btype :: BlockType
                    }

data BlockType = Solid
               | Lift Float Float Float Float Float Float Float Float  -- Lift my lw lth lph --> y = my + lw * cos (lth * t + lph)

blockSpeedX :: Float -> Block -> Float
blockSpeedX t (btype -> Solid) = 0
blockSpeedX t (btype -> Lift mx lwx lthx lphx _ _ _ _) = -lwx * lthx * sin(lthx * t + lphx)

blockSpeedY :: Float -> Block -> Float
blockSpeedY t (btype-> Solid) = 0
blockSpeedY t (btype -> Lift _ _ _ _ my lwy lthy lphy ) = lwy * lthy * cos(lthy * t + lphy)

blockAccelY :: Float -> Block -> Float
blockAccelY t (btype-> Solid) = 0
blockAccelY t (btype -> Lift _ _ _ _ my lwy lthy lphy ) = -lwy * lthy * lthy * sin(lthy * t + lphy)

data Ball = Ball {x :: Float, y :: Float, r :: Float, bounceType :: BounceType, 
                  vx :: Float, vy :: Float, ax :: Float, direction :: Direction, 
                  friction :: Float, slowdown :: Float, status :: AirGround }
            deriving Show 

data BounceType
  = NoBounce
  | Bounce   
    deriving Show

data Direction = DLeft | DRight 
  deriving Show

data AirGround = Air | Ground deriving (Eq, Show)

jumpspeed = 200
speed = 100
accel = 2000
gravity = -400
ground = -300
clearline = 2000
timelimit = 30
enemypoint = 100

timepoint :: Int
timepoint = 3


initWorld :: Picture -> Picture -> World
initWorld pict pictbg=
  World {ball = Ball {x = 0, y = 200, r = 13, bounceType = NoBounce, vx = 0, vy = 0, ax = 0,friction = 500, slowdown = 0.3, status = Air, direction = DRight } ,
         blocks = [Block{bx = 0, by = 0, bh = 30, bw = 100, btype = Solid},
                   Block{bx = 100, by = 10, bh = 40, bw = 10, btype = Solid},
                   Block{bx = 100, by = 90, bh = 10, bw = 100, btype = Solid },
                   Block{bx = 350, by = 0, bh = 30, bw = 100, btype = Solid},
                   Block{bx = 450, by = 10, bh = 40, bw = 10, btype = Solid},
                   Block{bx = 470, by = 20, bh = 50, bw = 10, btype = Solid},
                   Block{bx = 490, by = 30, bh = 60, bw = 10, btype = Solid},                                                        
                   Block{bx = 600, by = 30, bh = 60, bw = 10, btype = Solid},
                   Block{bx = 620, by = 20, bh = 50, bw = 10, btype = Solid},
                   Block{bx = 640, by = 10, bh = 40, bw = 10, btype = Solid}, 
                   Block{bx = 750, by = 0, bh = 30, bw = 100, btype = Solid},
                   Block{bx = 900, by = 0, bh = 10, bw = 30, btype = Lift 900 0 0 0 0 100 (-2) 0},
                   Block{bx = 1000, by = 0, bh = 10, bw = 30, btype = Lift 1000 0 0 0 0 100 (-2) 0.5},
                   Block{bx = 1100, by = 0, bh = 10, bw = 30, btype = Lift 1100 0 0 0 0 100 (-2) 1},
                   Block{bx = 1200, by = 0, bh = 30, bw = 50, btype = Solid},
                   Block{bx = 1300, by = 0, bh = 10, bw = 40, btype = Lift 1400 80 2 0 0 80 2 0},
                   Block{bx = 1500, by = 0, bh = 10, bw = 40, btype = Lift 1600 80 2 0 0 80 (-2) 0},
                   Block{bx = 1700, by = 0, bh = 10, bw = 40, btype = Lift 1800 80 2 0 0 80 2 0},
                   Block{bx = 500, by = 30, bh = 40, bw = 100, btype = Lift 30 80 1 0 30 80 1 0},
                   Block{bx = 2000, by = 0, bh = 2000, bw = 10, btype = Solid}
                   
                  ],                   
          enemies = [Ball{x = 240, y = 40, r = 10, bounceType = Bounce, vx = 20, vy = 50, ax = 0, friction = 0, slowdown = 1, status = Air, direction = DRight },
                     Ball{x = 250, y = 40, r = 10, bounceType = Bounce, vx = 20, vy = 100, ax = 0, friction = 0, slowdown = 1, status = Air, direction = DRight },
                     Ball{x = 280, y = 40, r = 10, bounceType = NoBounce, vx = 20, vy = 0, ax = 0, friction = 0, slowdown = 1, status = Air, direction = DRight },
                     Ball{x = -100, y = 300, r = 10, bounceType = NoBounce, vx = 20, vy = 0, ax = 0, friction = 0, slowdown = 1, status = Air, direction = DRight },
                     Ball{x = -150, y = 300, r = 10, bounceType = NoBounce, vx = 20, vy = 0, ax = 0, friction = 0, slowdown = 1, status = Air, direction = DRight }],
          passed = 0, pict = pict, pictbg = pictbg, score = 0, keyPressed = Nothing }
main :: IO ()
main = do
  Just pict <- loadJuicyPNG "obakeL.png"
  Just pictbg <- loadJuicyPNG "bg_mt_03.png"
  play (InWindow "Circle" (600, 500) (120, 100)) white 60
       (initWorld pict pictbg)
       renderWorld
       eventHandler
       tickHandler

renderWorld :: World -> Picture
renderWorld w
  | isClear (ball w) =
    let
      t = timelimit - (passed w)
      s = score w + round(t * fromIntegral timepoint)
    in translate (-300) 0 $ scale 0.2 0.2 $ pictures[text ("Clear! time left :" ++ show (t)),
                                                     translate 0 (-200) $ text (printf "score: %d timebonus: %d x %.2f" s timepoint t)]
  | otherwise = pictures[renderBackGround (x (ball w)) (pictbg w),
                         renderScore w,
                         translate (-x (ball w)) 0 $ pictures[renderMainBall (ball w) (pict w), renderBlocks (blocks w), renderEnemies (enemies w)]]

renderBackGround :: Float -> Picture -> Picture
renderBackGround x pictbg = pictures [translate (-xx) 0 $ scale 2 2 $ pictbg,
                                      translate ((-xx)+1024) 0 $ scale 2 2 $ pictbg]
  where
    xx = adjust x

adjust :: Float -> Float
adjust x | x < 0     = adjust (x + 1024)
         | x > 1024  = adjust (x - 1024)
         | otherwise = x

renderScore :: World -> Picture
renderScore w = translate 100 100 $ scale 0.5 0.5 $ text (show (score w))

renderMainBall :: Ball -> Picture -> Picture
renderMainBall ball pict =
  let f = case direction ball of 
        DLeft  -> id
        DRight -> scale (-1) 1 
  in translate (x ball) (y ball) $ f pict

  -- color red $ renderBall ball

renderBall :: Ball -> Picture
renderBall ball = translate (x ball) (y ball) $ circleSolid (r ball)

renderEnemies :: [Ball] -> Picture
renderEnemies es = color red $ pictures [renderBall e | e <- es]

renderBlock :: Block -> Picture
renderBlock b = translate (bx b) (by b) $ rectangleSolid (2*bw b) (2*bh b)

renderBlocks :: [Block] -> Picture
renderBlocks bls = pictures [renderBlock b | b <- bls]

eventHandler :: Event -> World -> World
-- eventHandler e _
--   | trace (show e) False = undefined
eventHandler (EventKey (MouseButton LeftButton) Down _ (mx, my)) w@World { ball = b } =
 w { ball = b { x = mx, y = my } }
eventHandler (EventKey (Char c) Down _ _) w@World { ball = b }
  | c == 'z' && status b == Ground = w{ ball = b{vy = (vy b) + jumpspeed }}
  | c == 'a' = w { ball = b { direction = DLeft}, keyPressed = Just DLeft } -- w{ball = b{ax = -accel, direction = DLeft}}
  | c == 's' = w { ball = b { direction = DRight }, keyPressed = Just DRight } -- w{ball = b{ax = accel, direction = DRight}}
eventHandler (EventKey (Char c) Up _ _) w@World { ball = b }
  | c `elem` "as" = w { keyPressed = Nothing }
--   | c == 'a' = w{ball = b{ax = 0}}
--   | c == 's' = w{ball = b{ax = 0}}
eventHandler _ w = w

tickHandler :: Float -> World -> World
tickHandler dt w
--  | trace (show $ ball w) False = undefined 
--  | trace (show (status $ ball w)) False = undefined
  | isGameOver (ball w) (enemies w) || isCrushed (ball w) (blocks w)= initWorld (pict w) (pictbg w)
  | isClear (ball w) = w --initWorld
  | otherwise = let startBall = (ball w) { status = Air, ax = case keyPressed w of
                                            Nothing -> 0
                                            Just DLeft -> -accel
                                            Just DRight -> accel }                                         
                    newBall = stepBall dt (interactBlocks (isJust $ keyPressed w) (passed w) startBall (blocks w))
                    newEnemies = [stepBall dt (interactBlocks True (passed w) (enemy {status = Air, ax = 0}) (blocks w)) | enemy <- (enemies w)]
                    newEnemies' = interactEnemies newBall newEnemies
                    newEnemies'' = filter (\e -> not $ isCrushed e (blocks w)) newEnemies'
                    newPassed = passed w + dt
                    newBlocks = [stepBlock newPassed block | block <- blocks w]
                in w{ball = newBall {vy = if length newEnemies' < length newEnemies then jumpspeed else vy newBall },
                     enemies = newEnemies'',
                     passed = newPassed,
                     blocks = newBlocks,
                     score = (score w) + enemypoint * (length newEnemies - length newEnemies')}

stepBlock :: Float -> Block -> Block
stepBlock t block@Block {btype = Solid} = block
stepBlock t block@Block {btype = Lift mx lwx lspx lphx my lw lsp lph} = block{bx = mx + lwx * (cos(lspx*t + lphx)),
                                                                             by = my + lw * (sin (lsp*t + lph)) }


limitSpeed :: Float -> Float -> Float
limitSpeed speed s = (-speed) `max` (speed `min` s)

stepBall :: Float -> Ball -> Ball
stepBall dt ball@Ball{..} = ball{y = y + dt*vy,
                                 vy = case status of
                                   Air -> vy + gravity*dt
                                   Ground -> vy + gravity*dt{- max 0 vy -},
                                 vx = case status of
                                   Air ->    limitSpeed speed (vx + 0.5 * ax*dt)
                                   Ground -> limitSpeed speed (vx + ax+dt), -- ((-speed) `max` (min speed (if ax == 0 then friction else 1) *vx + ax*dt)),
                                 x = x + dt*vx }

isInBlock :: Ball -> Block -> Bool
isInBlock ball@Ball{..} block@Block{..} =
  bx - bw - r <= x
  && x <= bx + bw + r
  && by - bh - r <= y 
  && y <= by + bh + r

isGameOver :: Ball -> [Ball] -> Bool
isGameOver ball enemies = any (\e -> ((x e)-(x ball))^2 + ((y e)-(y ball))^2 < ((r e) + (r ball))^2
                       && abs(atan2 ((y ball)-(y e)) ((x ball)-(x e)) - pi/2) > pi/4 ) enemies
                       || (y ball) <= ground

isClear :: Ball -> Bool
isClear ball = (x ball) > clearline

interactEnemies :: Ball -> [Ball] -> [Ball]
interactEnemies ball = filter (interactEnemy ball)
interactEnemy :: Ball -> Ball -> Bool
interactEnemy ball e = ((x e)-(x ball))^2 + ((y e)-(y ball))^2 > ((r e) + (r ball))^2
                       || abs(atan2 ((y ball)-(y e)) ((x ball)-(x e)) - pi/2) > pi/4

interactBlocks :: Bool -> Float -> Ball -> [Block] -> Ball
interactBlocks k t = foldl (interactBlock k t)

isCrushed :: Ball -> [Block] -> Bool
isCrushed ball blocks = (isTopHit && isBottomHit) || (isRightHit && isLeftHit)
  where
    hits = [doesHitBlock ball bl | bl <- blocks]
    isTopHit = not $ null $ [ () | AtTop <- hits ]
    isBottomHit = not $ null $ [ () | AtBottom <- hits]
    isRightHit = not $ null $ [ () | AtRight <- hits]
    isLeftHit = not $ null $ [ () | AtLeft <- hits]

data HitToBlock = AtLeft
                | AtRight
                | AtTop
                | AtBottom
                | Outside 

doesHitBlock :: Ball -> Block -> HitToBlock
doesHitBlock ball@Ball{..} block@Block{..}
  | not (isInBlock ball block) = Outside
  | atLeftPart   = AtLeft
  | atRightPart  = AtRight
  | atTopPart    = AtTop
  | otherwise    = AtBottom
  where
    horv = abs(x - bx)/bw > abs(y - by)/bh
    atLeftPart   = horv && x <= bx 
    atRightPart  = horv && x >  bx
    atTopPart    = not horv && y >= by
--    atBottomPart = not horv && y <  by 
  
interactBlock ::  Bool -> Float -> Ball -> Block -> Ball
interactBlock keyPressed t ball@Ball{r,slowdown,bounceType} block@Block{..} =
  case doesHitBlock ball block of
--  | trace (show (isInBlock ball block, atLeftPart, atRightPart, atTopPart, atBottomPart)) False = undefined
--  | not (isInBlock ball block) = ball
    AtLeft -> ball { x = bx - bw - r,
                     vx = slowdown*(-(vx ball)) }
    AtRight -> ball { x = bx + bw + r,
                      vx = slowdown*(-(vx ball)) }
    AtTop | diffVy < 0 -> --trace (show $ blockAccelY t block) $ 
             let y' = by + bh + r
                 diffY = abs (y ball - y')
                 ball' = case bounceType of
                   NoBounce ->　ball { {- y = y' - epsY, -} vy = max (vy ball) (blockSpeedY t block),
                                       status = Ground}
                   Bounce -> ball { {- y = y' ,-}
                                    vy = -slowdown * vy ball}
                 (ax', vx') =
                   let diffVx = blockSpeedX t block - vx ball'
                   in case {- (if friction ball < 0.98 then trace (show $ (diffVx, ax ball')) else id) $ -} status ball' of
--                   _ | abs (ax ball) > eps -> ax ball 
                     Ground | abs diffVx < eps, not keyPressed ->
                       (ax ball', blockSpeedX t block)
                     Ground | diffVx > 0 ->
                       (ax ball' + friction ball, vx ball')
                     Ground | diffVx < 0 ->
                       (ax ball' - friction ball, vx ball') -- vx ball' + friction ball * diffVx -- ax ball' - 10 
                     _ -> (ax ball', vx ball')
               in ball' { ax = ax', vx = vx' }
    AtBottom -> ball { y = by - bh - r,
                       vy = -vy ball }
    _ -> ball 
  where
    diffVy  = vy ball - blockVy
    blockVy = blockSpeedY t block 
    epsY = 0.01
    eps = 110
  -- where
  --   horv = abs(x - bx)/bw > abs(y - by)/bh
  --   atLeftPart   = horv && x <= bx 
  --   atRightPart  = horv && x >  bx
  --   atTopPart    = not horv && y >= by
                            
  
--  | otherwise = Ball{}
  -- where
  --   (newX, newVX) = if (x ball) < (bx block)
  --     then ((bx block)- (bw block) - (r ball), 
  --   (newY, newVY, newStatus) = undefined
      
    

    

  
